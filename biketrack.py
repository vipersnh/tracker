import os
import datetime
import time
import urllib
from google.appengine.api import users
from google.appengine.ext import db
import webapp2
import jinja2

JINJA_ENVIRONMENT = jinja2.Environment(
	loader = jinja2.FileSystemLoader(os.path.dirname(__file__)),
	extensions = ['jinja2.ext.autoescape'])


class bikeList(db.Model):
	ownerId = db.StringProperty()
	bikeId = db.StringProperty()
	bikeName = db.StringProperty()
	bikeModel = db.StringProperty()
	bikeMileage = db.FloatProperty()
	bikeRTONumber = db.StringProperty()
	dateStamp = db.DateProperty() ## Holds the date of creation of this new bike item

class bikeToken(db.Model):
	ownerId = db.StringProperty()
	bikeId = db.StringProperty()
	latitude = db.FloatProperty()
	longitude = db.FloatProperty()
	timeStamp = db.DateTimeProperty() ## Holds date and time property
	bikeSpeed = db.FloatProperty()	## Holds the speed reported by GPS at the time of reporting

class MainPage(webapp2.RequestHandler):
	def get(self):
		if users.get_current_user():
			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('index.html')
			self.response.write(template.render(template_values))
		else:
			self.redirect(users.create_login_url(self.request.uri))

class add_new_bike(webapp2.RequestHandler):
	def get(self):
		if users.get_current_user():
			user = users.get_current_user()
			nickname = user.nickname().split("@")
			template_values = {'user_nickname': nickname[0],'size': len(nickname[0])+1}
			template = JINJA_ENVIRONMENT.get_template('add_new_bike.html')
			self.response.write(template.render(template_values))
		else:
			 self.redirect("/")

	def post(self):
		all_valid = True
		user = users.get_current_user()
		## Add additional code to validate the input
		param1 = self.request.get("bikeEmailId").lower().split("@")[0]
		param2 = self.request.get("bikeName").lower()
		param3 = self.request.get("bikeModel").lower()
		param4 = float(self.request.get("bikeMileage"))
		param5 = self.request.get("bikeNumber").lower()
		param6 = datetime.date.today()
		querry = bikeList.all()
		for pair in querry.run():
			if param1 == pair.bikeId:
				all_valid = False
		if all_valid:
			bikeListItem = bikeList(ownerId = user.nickname(),bikeId = param1, bikeName = param2, bikeModel = param3, bikeMileage = param4, bikeRTONumber = param5,dateStamp = param6)
			bikeListItem.put()
			self.redirect("/")
		else:
			self.response.write("Something went wrong here")

class BikeLogin(webapp2.RequestHandler):
	def get(self):
		template_values = {}
		template = JINJA_ENVIRONMENT.get_template('bike_login.html')
		self.response.write(template.render(template_values))

	def post(self):
		all_valid = True
		bikeId_temp = self.request.get("bikeAccount")
		querry = bikeList.all()
		querry.filter("bikeId =", bikeId_temp)
		result = querry.get()
		if result is not None:
			param_ownerId = result.ownerId
			param_bikeId = bikeId_temp
			## TODO : Add additional code to validate the inputs
			param_latitude = float(self.request.get("latitude"))
			param_longitude = float(self.request.get("longitude"))
			param_dateStamp = self.request.get("dateStamp").lower().split(" ")
			param_day = int(param_dateStamp[0])
			param_mon = int(param_dateStamp[1])
			param_year = int(param_dateStamp[2])
			param_timeStamp = self.request.get("timeStamp").lower().split(" ")
			param_hour = int(param_timeStamp[0],10)
			param_min = int(param_timeStamp[1],10)
			param_sec = int(param_timeStamp[2],10)
			param_timeStamp = datetime.datetime(param_year,param_mon,param_day,param_hour,param_min,param_sec)
			param_speed = float(self.request.get("speed"))
			bikeTokenItem = bikeToken(ownerId = param_ownerId,bikeId = param_bikeId,latitude = param_latitude,longitude = param_longitude,timeStamp = param_timeStamp,bikeSpeed = param_speed)
			bikeTokenItem.put()
			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('bike_login.html')
			self.response.write(template.render(template_values))
			
					
			
		

class OwnerPage(webapp2.RequestHandler):
	def get(self):
		if users.get_current_user():
			user = users.get_current_user()
			template_values = {}
			template = JINJA_ENVIRONMENT.get_template('owner_page.html')
			self.response.write(template.render(template_values))	
		else:
			self.redirect(users.create_login_url(self.request.uri))

	def post(self):
		pass

class details(webapp2.RequestHandler):

	def get(self):
			user = users.get_current_user()
			querry = bikeToken.all()
			querry.filter('ownerId =',"vipersnh")
			querry.order("timeStamp")
			pointsList = []
			pointsListList = []
			bikeSpeedList = []
			bikeSpeedListList = []
			pathStatistics = []
			finalStatistics = []
			curr  = None
			prev = None	
			for token in querry.run():
				if prev is None:
					prev = token.timeStamp;
				else:
					delta = token.timeStamp-prev
					prev = token.timeStamp
					if abs(delta.seconds) > 2:
						pointsListList.append(pointsList)
						bikeSpeedListList.append(bikeSpeedList)
						pointsList = []
						prev = None;
				lat = token.latitude;
				lon = token.longitude;
				pair = [lat,lon]
				pointsList.append(pair)
				bikeSpeedList.append(token.bikeSpeed)
			if len(pointsList):
				pointsListList.append(pointsList)
				bikeSpeedListList.append(bikeSpeedList)
			
				
			template_values = {	"owner_name": user.nickname(),
						"bike_name": "Some bike",
						'pointsListList':pointsListList,
						'bikeSpeedListList' : bikeSpeedListList,
					  }
			template = JINJA_ENVIRONMENT.get_template('details.html')
			self.response.write(template.render(template_values))	
				
	
	def post(self):
		pass
		

class debug(webapp2.RequestHandler):
	def get(self):
		if users.get_current_user():
			user = users.get_current_user()
			if user.nickname() == "vipersnh":
				self.response.write(" Entering debug mode <br> <ol>")
				allUsers = OwnerBikeAccountPair.all()
				for i in allUsers:
					string = "<li>"
					string += i.ownerId+"+"
					string += i.bikeId+"+"
					string += i.bikeName+"+"
					string += i.bikeModel+"+"
					string += str(i.bikeMileage)+"+"
					string += i.bikeRTONumber+"+"
					string += str(i.timeStamp)+"+"
					string += "</li>"
					self.response.write(string)
				self.response.write("</ol> <br>All done")
					
			else:
				self.response.write(" Invalid URL \n"+user.nickname())
		else:
			self.redirect(users.create_login_url(self.request.uri))

class view_bikes_registered(webapp2.RequestHandler):
	def get(self):
		if users.get_current_user():
			user = users.get_current_user()
			ownerId_temp = user.nickname()
			querry = bikeList.all()
			querry.filter("ownerId =", ownerId_temp)
			self.response.write(" Fetching all registered bike values <br> <ol> ")
			for pair in querry.run():
				string = "<li>"
				string += " "+pair.bikeId
				string += " "+pair.bikeName
				string += " "+pair.bikeModel
				string += " "+str(pair.bikeMileage)
				string += " "+pair.bikeRTONumber
				string += "</li>"
				self.response.write(string)
			

			self.response.write("</ol> <br> All done")
		else:
			self.response.write("Some thing went wrong here 2")
				
	
	def post(self):
		pass

class logout(webapp2.RequestHandler):
	def post(self):
		if users.get_current_user():
			self.redirect(users.create_logout_url("http://vipersnh.appspot.com"))

app = webapp2.WSGIApplication([
    ('/', MainPage),('/add_new_bike.html',add_new_bike),('/bike_login.html',BikeLogin),('/owner_page.html',OwnerPage),('/debug.html',debug),('/view_bikes_registered.html',view_bikes_registered),('/logout.html',logout),('/details.html',details),], debug=True)
